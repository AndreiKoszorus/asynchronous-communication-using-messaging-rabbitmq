package reciever;

import Services.Notifications;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class EmailReciever {

    private static final String EXCHANGE_NAME = "DvDQueue";
    private Notifications notifications;

    public EmailReciever(){
        notifications = new Notifications();
        notifications.addSubscriber("sd.proiect.3.2@gmail.com");
        notifications.addSubscriber("sd.proiect.3.2@gmail.com");
        notifications.addSubscriber("sd.proiect.3.2@gmail.com");
    }

    public void recieveDvd() throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME,"fanout");
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName,EXCHANGE_NAME,"");
        System.out.println("Waiting for messages(email)");

        Consumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Email sent");
                notifications.notifySubscribers(message);

            }

        };
        channel.basicConsume(queueName,true,consumer);

    }
}
