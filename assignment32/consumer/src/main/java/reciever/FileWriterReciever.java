package reciever;

import Services.FileService;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class FileWriterReciever {

    private static final String EXCHANGE_NAME = "DvDQueue";
    private FileService file;

    public FileWriterReciever(){
        file = new FileService();
    }

    public void recieveDvd() throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME,"fanout");
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName,EXCHANGE_NAME,"");
        System.out.println("Waiting for messages(file)");
        Consumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("File created");
                file.createDvdFile(message);

            }

        };

        channel.basicConsume(queueName,true,consumer);

    }
}
