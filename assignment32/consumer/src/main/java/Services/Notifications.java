package Services;

import com.producer.models.DVD;

import java.util.ArrayList;
public class Notifications {

    private static ArrayList<String> subscribers;
    private MailService mailService;

    public Notifications(){
        subscribers = new ArrayList<String>();
        mailService = new MailService("sd.proiect.3.2@gmail.com","proiect_3");
    }

    public void addSubscriber(String subscriber){
        subscribers.add(subscriber);
    }

    public void notifySubscribers(String dvd){

        for(String email:subscribers){
            mailService.sendMail(email,"New DVD",dvd);
        }

    }




}
