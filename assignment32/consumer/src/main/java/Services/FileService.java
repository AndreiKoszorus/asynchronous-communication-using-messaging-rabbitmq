package Services;

import com.producer.models.DVD;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class FileService {

    public void createDvdFile(String dvd) throws FileNotFoundException, UnsupportedEncodingException {

        String[] parts = dvd.split("\\n");
        String[] parts2 = parts[0].split(":");
        PrintWriter writer = new PrintWriter(parts2[1],"UTF-8");
        writer.println(dvd);
        writer.close();
    }
}
