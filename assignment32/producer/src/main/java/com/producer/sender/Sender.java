package com.producer.sender;

import com.producer.models.DVD;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Sender {
    private final static String EXCHANGE_NAME = "DvDQueue";

    public void sendMessage(DVD dvd) throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME,"fanout");

        channel.basicPublish(EXCHANGE_NAME,"",null,dvd.toString().getBytes("UTF-8"));

        System.out.println("DVD sent: "+dvd.toString());
        channel.close();
        connection.close();
    }


}
