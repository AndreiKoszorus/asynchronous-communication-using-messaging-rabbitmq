package com.producer.view;

import com.producer.models.DVD;
import com.producer.sender.Sender;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class ViewController extends Application {
    @FXML
    private TextField titleField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField yearField;

    private Sender sender;

    public ViewController(){
        sender = new Sender();
    }


    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "E://assignment3//assignment32//producer//src//main//java//com//producer//view//DVDView.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

        // Create the Pane and all Details
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        primaryStage.setScene(scene);
        // Set the Title to the Stage
        primaryStage.setTitle("Create DVD");
        // Display the Stage
        primaryStage.show();

    }

    public void createDvd() throws Exception{
        String title = titleField.getText();
        int year = Integer.parseInt(yearField.getText());
        double price = Double.parseDouble(priceField.getText());

        DVD dvd = new DVD(year,price,title);

        sender.sendMessage(dvd);
    }
}
